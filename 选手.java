package test;
import java.util.Arrays;
import java.util.Scanner;
public class test {
	public static void main(String[] args) {
		Scanner people = new Scanner(System.in);
		Score reference = new Score();
		String no, name;
		System.out.println("请输入选手人数："); // 输入选手数
		int num = people.nextInt();
		Player[] player = new Player[num];// 根据选手数，调用含参构造方法创建选手对象数组player ，分配内存空间
		System.out.println("请输入评委人数：");
		int number = people.nextInt();// 输入评委数
		// 根据评委数，调用含参构造方法创建打分类对象
		for (int i = 0;i <num;i++) // 利用循环结构给每个选手打分
		{
			player[i] = new Player();
			System.out.println("请输入输入选手编号：");
			no = people.next(); // 输入选手编号
			System.out.println("请输入输入选手姓名：");
			name = people.next(); // 输入选手姓名
			reference.Player(number);
			System.out.printf("请为%d号选手打分：\n", i); // 调用含有编号和姓名参数的构造方法对player数组元素player[i]进行实例化
			reference.inputScore(); // 调用打分类对象的打分方法inputScore对选手打分
			double sco=reference.average();
			player[i] = new Player(name,no,sco);
			System.out.println(no + "最终得分为" + reference.average());
			// 调用打分类中计算最后总分的方法得到该选手的最后得分
		}
		// 对选手按照得分排序，输出排行榜
		for(int i=0;i<player.length;i++) {
			System.out.println(player[i].toString());
		}
	}
}
package test;
public class Player implements Comparable<Player> {
	private String name; // 声明成员变量 编号，姓名，得分
	private String no;
	private double score;
	public Player() {
		
	}
	public Player(String name, String no, double score) { // 含有参数 编号、姓名的构造方法
		this.setNo(no); // 设置编号
		this.setName(name); // 设置姓名
		this.setscore(score); // 设置成绩
	}
	public void setNo(String n) { // 设置选手编号
		no = n;
	}
	public void setName(String n) { // 设置选手姓名
		name = n;
	}
	public void setscore(double score2) { // 设置选手编号
		score = score2;
	}
	public String getNo() { // 取得选手编号
		return no;
	}
	public String getName() { // 取得选手姓名
		return name;
	}
	public double getscore() { // 取得选手成绩
		return score;
	}

	public String toString() { // toString（）方法输出选手的所有属性信息
		return "排名榜：" + "\n" + "姓名：" + this.name + "，编号：" + this.no + "，最终得分" + this.score;
	}
	public int compareTo(Player o) { // 对选手的最后得分进行比较
		if (this.score > o.score) {
			return -1;
		} else if (this.score < o.score) {
			return 1;
		} else
			return 0;
	}
}
package test;
import java.util.Scanner;
//打分类
public class Score {
    private double[] score=null;//评委给出的分数score（数组）（注意，此时不能对数组分配空间）
    private int number;  //评委人数
    public void Player(int number) {     //含有一个参数（评委人数）的构造方法
		this.setnum(number);   //设置评委人数
}  
    public void setnum(int n){   //设置评委人数
   	  number=n;
   	  }
    public void setscore(double n[]){   //设置评委给出分数
     	  score=n;
     	  }
    public int getnum(){    //取得评委人数
  	  return number;
  	  }
    public double[] getscore(){    //取得评委给出分数
    	  return score;
    	  }
	public  void inputScore() {  //打分方法，此方法中首先需要对成员变量分数score数组分配内存空间，//然后完成每个评分的打分。分数从键盘输入	
		double score1[]=new double[this.number];
		Scanner num=new Scanner(System.in);
		for(int i=0;i<score1.length;i++)
		{
			 score1[i]=num.nextDouble();
		}
		score=score1;
	}
       //定义方法，计算评委打分的最后分数（去掉一个最高分，一个最低分后求平均值），此方法应该有返回值，返回最后得分
	public double average() {
		inputScore();
		double sum = 0;
		double ave;    
		double max=score[0];
		for(int i=0;i<score.length;i++)
		{
			if(max<score[i])
			{
				max=score[i];
			}
		}
		System.out.printf("去掉一个最高分%.2f：\n",max);
		double min=score[0];
		for( int i=0;i<score.length;i++)
		{
			if(min>score[i])
			{
				min=score[i];
			}
		}
		System.out.printf("去掉一个最低分%.2f：\n",min);
		for(int i=0;i<score.length;i++)
		{
			sum+=score[i];
		}
		sum=sum-(max+min);
		ave=(double)sum/(number-2);
		return ave;
	}
}

